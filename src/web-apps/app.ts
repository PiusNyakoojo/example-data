/*
App data is related to web applications that are deployed to a web host provider.

Such applications include:
- Single Page Applications (SPA)
- Node.js, Golang, Ruby on rails
- And other backend solutions for building web apps

App data is managed separately from Game data to make things cleaner and clearer.

Games may have multiple Apps: one for production, one for development or maybe
even a demo build that doesn't have the full features of the production app.
Demo apps could be useful for enabling a try-before-buy model for developers.
*/

class App {
  did: string = ''
  uid: string = ''
  aid: string = ''
  title: string = ''
  description: string = ''
  dateCreated: Date = new Date()
  game: { uid: string, gid: string } = { uid: '', gid: '' }
  publisher: { uid: string, username: string } = { uid: '', username: '' }

  status: string = ''
  type: string = ''

  fileHost: AppFileHost = new AppFileHost()
  webHostDetails: AppWebHostDetails = new AppWebHostDetails()
}

class AppFileHost {
  providerUID: string = ''
  repoID: string = ''
  name: string = ''
  repoURL: string = ''
  branch: string = ''
  tarball: string = ''
}

class AppWebHostDetails {
  providerUID: string = ''
  appID: string = ''
  webURLList: string[] = []
  appSetupID: string = ''
}

class AppWebHostDetailsStatus {
  label: string = ''
  message: string = ''
  buildLogDetails: string = ''
  runtimeLogDetails: string = ''
}

const APP_TYPE_SPA: string = 'single-page-app'
const APP_TYPE_NODEJS: string = 'node-js'
const APP_TYPE_GOLANG: string = 'go-lang'

const APP_GIT_FILEHOST_PROVIDER_GITLAB: string = 'com-gitlab-www'

const APP_WEBHOST_PROVIDER_DATARCHIVE: string = 'dat-archive'
const APP_WEBHOST_PROVIDER_WEBTORRENT: string = 'webtorrent'
const APP_WEBHOST_PROVIDER_HEROKU: string = 'com-heroku-www'

const APP_STATUS_PENDING: string = 'pending'
const APP_STATUS_ACTIVE: string = 'active'
const APP_STATUS_ERROR: string = 'error'
const APP_STATUS_DELETED: string = 'deleted'

export {
  App,
  AppFileHost,
  AppWebHostDetails,
  AppWebHostDetailsStatus,
  APP_TYPE_SPA,
  APP_TYPE_NODEJS,
  APP_TYPE_GOLANG,
  APP_GIT_FILEHOST_PROVIDER_GITLAB,
  APP_WEBHOST_PROVIDER_DATARCHIVE,
  APP_WEBHOST_PROVIDER_WEBTORRENT,
  APP_WEBHOST_PROVIDER_HEROKU,
  APP_STATUS_PENDING,
  APP_STATUS_ACTIVE,
  APP_STATUS_ERROR,
  APP_STATUS_DELETED
}
