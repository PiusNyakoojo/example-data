
// 'width:height': 'imageURL'

const data: Array<{ [imageSize: string]: string }> = [

  // Total War: THREE KINGDOMS
  {
    '116:44': 'https://steamcdn-a.akamaihd.net/steam/apps/779340/capsule_231x87.jpg?t=1564668211',
    '324:151': 'https://steamcdn-a.akamaihd.net/steam/apps/779340/header.jpg?t=1564668211'
  },
  // Total War: WARHAMMER II
  {
    '116:44': 'https://steamcdn-a.akamaihd.net/steam/apps/594570/capsule_231x87.jpg?t=1558606539',
    '324:151': 'https://steamcdn-a.akamaihd.net/steam/apps/594570/header.jpg?t=1558606539'
  },
  // My Little Army
  {
    '116:44': 'https://steamcdn-a.akamaihd.net/steam/apps/1068220/capsule_231x87.jpg?t=1564778002',
    '324:151': 'https://steamcdn-a.akamaihd.net/steam/apps/1068220/header.jpg?t=1564778002'
  },
  // Siege Machines Builder
  {
    '116:44': 'https://steamcdn-a.akamaihd.net/steam/apps/1067760/capsule_231x87.jpg?t=1563972240',
    '324:151': 'https://steamcdn-a.akamaihd.net/steam/apps/1067760/header.jpg?t=1563972240'
  },
  // Divinity: Original Sin 2 - Definitive Edition
  {
    '116:44': 'https://steamcdn-a.akamaihd.net/steam/apps/435150/capsule_231x87.jpg?t=1561485554',
    '324:151': 'https://steamcdn-a.akamaihd.net/steam/apps/435150/header.jpg?t=1561485554'
  },
  // Fire And Thunder
  {
    '116:44': 'https://steamcdn-a.akamaihd.net/steam/apps/1106660/capsule_231x87.jpg?t=1563970354',
    '324:151': 'https://steamcdn-a.akamaihd.net/steam/apps/1106660/header.jpg?t=1563970354'
  },
  // Baldur's Gate: Enhanced Edition
  {
    '116:44': 'https://steamcdn-a.akamaihd.net/steam/apps/228280/capsule_231x87.jpg?t=1547670081',
    '324:151': 'https://steamcdn-a.akamaihd.net/steam/apps/228280/header.jpg?t=1547670081'
  },
  // Project Winter
  {
    '116:44': 'https://steamcdn-a.akamaihd.net/steam/apps/774861/capsule_231x87.jpg?t=1565010091',
    '324:151': 'https://steamcdn-a.akamaihd.net/steam/apps/774861/header.jpg?t=1565010091'
  },
  // Call of Duty®: Black Ops II
  {
    '116:44': 'https://steamcdn-a.akamaihd.net/steam/apps/202970/capsule_231x87.jpg?t=1492633735',
    '324:151': 'https://steamcdn-a.akamaihd.net/steam/apps/202970/header.jpg?t=1492633735'
  },
  // Warface
  {
    '116:44': 'https://steamcdn-a.akamaihd.net/steam/apps/291480/capsule_231x87.jpg?t=1562929790',
    '324:151': 'https://steamcdn-a.akamaihd.net/steam/apps/291480/header.jpg?t=1562929790'
  },
  // My Little Army
  {
    '116:44': '',
    '324:151': ''
  },
  // My Little Army
  {
    '116:44': '',
    '324:151': ''
  },
  // My Little Army
  {
    '116:44': '',
    '324:151': ''
  }
]

export default data
