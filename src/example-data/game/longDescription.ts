/* tslint:disable */

const data: string[] = [
  // Total War: THREE KINGDOMS (https://store.steampowered.com/app/779340/Total_War_THREE_KINGDOMS/)
  `Total War: THREE KINGDOMS is the first in the multi award-winning strategy series to recreate epic conflict across ancient China. Combining a gripping turn-based campaign game of empire-building, statecraft and conquest with stunning real-time battles, Total War: THREE KINGDOMS redefines the series in an age of heroes and legends.
China in 190CE

Welcome to a new era of legendary conquest.

This beautiful but fractured land calls out for a new emperor and a new way of life. Unite China under your rule, forge the next great dynasty, and build a legacy that will last through the ages.
Choose from a cast of 12 legendary Warlords and conquer the realm. Recruit heroic characters to aide your cause and dominate your enemies on military, technological, political, and economic fronts.

Will you build powerful friendships, form brotherly alliances, and earn the respect of your many foes? Or would you rather commit acts of treachery, inflict heart-wrenching betrayals, and become a master of grand political intrigue?

Your legend is yet to be written, but one thing is certain: glorious conquest awaits.
ANCIENT CHINA RECREATED

Discover Three Kingdoms China, a land of breath-taking natural beauty. Battle across lush subtropics, arid deserts and snow-capped mountains. Marvel at legendary landmarks like the Great Wall of China and the Yangtze River. Explore the length and breadth of ancient China as you restore harmony to its embattled landscape.
CHINA’S GREATEST LEGENDS

Forge a new empire as one of 12 legendary Warlords drawn from China’s celebrated historical epic, the Romance of the Three Kingdoms. Peerless commanders, powerful warriors and eminent statesmen, these characters each have a unique playstyle and objectives. Recruit an epic supporting cast of heroes to command your armies, govern your provinces and strengthen your growing empire. Characters are the beating heart of the game, and China’s very future will be shaped by its champions.
GUANXI SYSTEM

Modelled on Guanxi, the Chinese concept of dynamic inter-relationships, Total War: THREE KINGDOMS takes a paradigm-shifting approach to character agency, with iconic, larger-than-life heroes and their relationships defining the future of ancient China. Each of these characters is brought to life with their own unique personality, motivations, and likes/dislikes. They also form their own deep relationships with each other, both positive and negative, that shape how your story plays out.
ARTISTIC PURITY

With stunning visuals and flamboyant Wushu combat, THREE KINGDOMS is the art of war. With beautiful UI, vibrant vistas and authentic Chinese-inspired artwork, this reimagining of ancient China is a visual feast.
REAL-TIME & TURN-BASED HARMONY

The turn-based campaign and real-time battles of Total War: THREE KINGDOMS are more interconnected than ever before. Actions in battle now have much greater consequences, affecting your Heroes’ relationship towards you, as well as the friendships and rivalries they develop with other characters. In a world where powerful allies are one of the keys to success, this adds a brand-new element to how victory is achieved.`,

  // Total War: WARHAMMER II (https://store.steampowered.com/app/594570/Total_War_WARHAMMER_II/)
  `DEFEND YOUR WORLD. DESTROY THEIRS.
Total War: WARHAMMER II is a strategy game of titanic proportions. Choose from four unique, varied factions and wage war your way – mounting a campaign of conquest to save or destroy a vast and vivid fantasy world.

This is a game of two halves – one a turn-based open-world campaign, and the other intense, tactical real-time battles across the fantastical landscapes of the New World.

Play how you choose – delve into a deep engrossing campaign, experience unlimited replayability and challenge the world in multiplayer with a custom army of your favourite units. Total War: WARHAMMER II offers hundreds of hours of gameplay and no two games are the same.


World-Spanning Conquest
Engage in statecraft, diplomacy, exploration and build your empire, turn by turn. Capture, build and manage teeming settlements and recruit vast armies. Level up Legendary Lords and Heroes and arm them with mythical weapons and armour. Negotiate alliances or declare Total War to subjugate any that stand between you and your goal.

Epic Real-Time Battles
Command thousands-strong legions of soldiers in intense tactical battles. Send forth ferocious, twisted monsters, fire-breathing dragons and harness powerful magic. Utilise military strategies, lay ambushes, or use brute force to turn the tide of combat and lead your forces to victory.
The second in a trilogy and sequel to the award-winning Total War: WARHAMMER, Total War: WARHAMMER II brings players a breath-taking new narrative campaign, set across the vast continents of Lustria, Ulthuan, Naggaroth and the Southlands. The Great Vortex Campaign builds pace to culminate in a definitive and climactic endgame, an experience unlike any other Total War title to date.

Playing as one of 8 Legendary Lords across 4 iconic races from the world of Warhammer Fantasy Battles, players must succeed in performing a series of powerful arcane rituals in order to stabilise or disrupt The Great Vortex, while foiling the progress of the other races.

Each Legendary Lord has a unique geographical starting position, and each race offers a distinctive new playstyle with unique campaign mechanics, narrative, methods of war, armies, monsters, Lores of Magic, legendary characters, and staggering new battlefield army abilities.

Shortly after launch, owners of both the original game and Total War™ WARHAMMER II will gain access to the colossal third campaign. Exploring a single open-world epic map covering the Old World and the New World, players may embark on monumental campaigns as any owned Race from both titles.


Conquer the world, together
Each of the Races in Total War™ WARHAMMER II will be playable in single and multiplayer campaign, plus custom and multiplayer battles. As the two Legendary Lords for each race all have their own unique campaign start positions, you’ll be able to play a 2-player co-op campaign as the same race. If you own both parts 1 and 2, you’ll be able to play in multiplayer as any of the races you own.


The World of Total War: WARHAMMER II
Millennia ago, besieged by a Chaos invasion, a conclave of High Elf mages forged a vast, arcane vortex. Its purpose was to draw the Winds of Magic from the world as a sinkhole drains an ocean, and blast the Daemonic hordes back to the Realm of Chaos. Now the Great Vortex falters, and the world again stands at the brink of ruin.

Powerful forces move to heal the maelstrom and avert catastrophe. Yet others seek to harness its terrible energies for their own bitter purpose. The race is on, and the very fate of the world will lie in the hands of the victor.

Prince Tyrion, Defender of Ulthuan, guides the High Elves in their desperate efforts to stabilise the vortex as it roils above their home continent.

Atop his palanquin-throne, the Slann Mage-Priest Mazdamundi directs his Lizardmen war-hosts as they surge northward from Lustria. He, too, is intent on preventing cataclysm, though the methods of The Old Ones must prevail.

The Witch King Malekith and his sadistic Dark Elf hordes spew forth from Naggaroth and their labyrinthine Black Arks. He tastes great weakness in the vortex – and great opportunity in its demise.

Meanwhile the Skaven, led by Queek Headtaker, stir in their foetid subterranean tunnels. There they multiply unchecked and look hungrily towards the surface, their motives obscured. The time for revelation is nigh…

Four races, four outcomes, a single goal: control of the Great Vortex, for good or ill.`,

// My Little Army (https://store.steampowered.com/app/1068220/My_Little_Army/)
`Resurrect your undead skeleton army, equip them with ancient artifacts and become the greatest boss like no one before. Kill good guys, bad guys, eradicate or enslave other monster clans. Your goal is to earn the title of the Greatest Evil. Does this sound badass? Better read mode.

You will be able to command army by your own voice. Yeah, its possible. You can shout, yell, scream at your minions and it works! I mean that they become even more angry, deal more damage and start act like a total fools... Well, sometimes it's not the best way out because ability to recrut more minions is limited. Better take care of them.

During the game you will find lots of random items that will allow you to improve your army. Thanks to them you will be able to change your poor little servants into machines of destruction! `,

// Siege Machines Builder (https://store.steampowered.com/app/1067760/Siege_Machines_Builder/)
`Use photorealistic graphics to feel like a builder of siege machines! Get raw materials and craft parts using tools.
Build and test in combat historical siege machines - ancient and medieval.
Destroy castles and towns. Manage the siege choosing the best way to capture the stronghold.
Spend money on the development of the workshop.`,

// Divinity: Original Sin 2 - Definitive Edition (https://store.steampowered.com/app/435150/Divinity_Original_Sin_2__Definitive_Edition/)
`The Divine is dead. The Void approaches. And the powers lying dormant within you are soon to awaken. The battle for Divinity has begun. Choose wisely and trust sparingly; darkness lurks within every heart.

Who will you be?
A flesh-eating Elf, an Imperial Lizard or an Undead, risen from the grave? Discover how the world reacts differently to who - or what - you are.
It’s time for a new Divinity!
Gather your party and develop relationships with your companions. Blast your opponents in deep, tactical, turn-based combat. Use the environment as a weapon, use height to your advantage, and manipulate the elements themselves to seal your victory.
Ascend as the god that Rivellon so desperately needs.
Explore the vast and layered world of Rivellon alone or in a party of up to 4 players in drop-in/drop-out cooperative play. Go anywhere, unleash your imagination, and explore endless ways to interact with the world. Beyond Rivellon, there’s more to explore in the brand-new PvP and Game Master modes.

    Choose your race and origin. Choose from 6 unique origin characters with their own backgrounds and quests, or create your own as a Human, Lizard, Elf, Dwarf, or Undead. All choices have consequences.

    Unlimited freedom to explore and experiment. Go anywhere, talk to anyone, and interact with everything! Kill any NPC without sacrificing your progress, and speak to every animal. Even ghosts might be hiding a secret or two…

    The next generation of turn-based combat. Blast your opponents with elemental combinations. Use height to your advantage. Master over 200 skills in 12 skill schools. But beware - the game’s AI 2.0 is our most devious invention to date.

    Up to 4 player online and split-screen multiplayer. Play with your friends online or in local split-screen with full controller support.

    Game Master Mode: Take your adventures to the next level and craft your own stories with the Game Master Mode. Download fan-made campaigns and mods from Steam Workshop.

    4K Support: an Ultimate 4K experience pushing RPGs into a new era!`,

// Fire And Thunder (https://store.steampowered.com/app/1106660/Fire_And_Thunder/)
`Become a Greek-like hero and set off in your own odyssey! Kill monsters from Greek and Babylon mythology, pillage gods palaces and dungeons, save women and claim their riches. Let the FIRE and THUNDER leads you on your way!

    Use magical artefacts to slaughter enemies.
    Discover new weapons and their powers.
    Defeat gods and their boss puppets.
    Unleash unstoppable power by using your own, custom combination of artifacts.
    Explore the ancient cities and dungeons.
    Travel through mysterious land of gods.
    Admire the appearance of structures inspired by Greek and Babylon architecture.`,

// Baldur's Gate: Enhanced Edition (https://store.steampowered.com/app/228280/Baldurs_Gate_Enhanced_Edition/)
`Forced to leave your home under mysterious circumstances, you find yourself drawn into a conflict that has the Sword Coast on the brink of war. Soon you discover there are other forces at work, far more sinister than you could ever imagine...

Since its original release in 1998, Baldur’s Gate has set the standard for Dungeons & Dragons computer roleplaying games. Customize your hero, recruit a party of brave allies, and explore the Sword Coast in your search for adventure, profit… and the truth.

Running on an upgraded and improved version of the Infinity Engine, Baldur’s Gate: Enhanced Edition includes the original Baldur’s Gate adventure, the Tales of the Sword Coast expansion, and all-new content including three new party members.
Key Features

    The original Baldur’s Gate adventure
    Tales of the Sword Coast expansion
    New Adventure: The Black Pits
    New Characters: The Blackguard Dorn Il-Khan, Neera the Wild Mage, and Rasaad yn Bashir the Monk
    New player character voice sets
    Native support for high-resolution widescreen displays
    Over 400 improvements to the original game
    Improved multiplayer support with connectivity between all platforms`,

// Project Winter (https://store.steampowered.com/app/774861/Project_Winter/)
`Betray your friends in this 8 person multiplayer focused on social deception and survival.

Communication and teamwork is essential to the survivors’ ultimate goal of escape. Gather resources, repair structures, and brave the wilderness together. Just beware that there are traitors within the group who will be steadily gaining strength as the match progresses. Traitors only goal is to stop the survivors by any means necessary.

Features:

Escape to Survive
As a survivor amongst a group of strangers you will have to collect resources, battle the elements and complete a series of tasks in order to call in one of several rescue vehicles.

Hidden Roles
Amongst the group of survivors, lurk unidentified traitors. The traitors are known to each other but not the survivors. Their goal is to stop the survivors from escaping without being identified and killed.

Teamwork
Survivors cannot escape unless they work together. Players who wander off on their own will have difficulty surviving the elements, can fall victim to hostile wildlife, or become easy targets for the traitors.

Communication
There are several ways to communicate with other players, including proximity-based voice chat, private voice chat radio channels, text chat, and emotes - communication is key if the survivors have any hope of completing their tasks and defending against the traitors.

Betrayal & Deception
The traitors are outnumbered and weak when the game begins, they may infiltrate the survivors and earn their trust while they build up their strength. Survivors can never be 100% sure of who to trust. The traitors can take advantage of this by spreading lies, and pitting the survivors against each other.`,

// Call of Duty®: Black Ops II (https://store.steampowered.com/app/202970/Call_of_Duty_Black_Ops_II/)
`Pushing the boundaries of what fans have come to expect from the record-setting entertainment franchise, Call of Duty®: Black Ops II propels players into a near future, 21st Century Cold War, where technology and weapons have converged to create a new generation of warfare. `,

// Warface (https://store.steampowered.com/app/291480/Warface/)
`Warface is a contemporary MMO first person shooter with millions of fans around the world. It offers intense PvP modes, compelling PvE missions and raids that you can challenge with four diverse classes and a colossal customizable arsenal.

Intense PvP
Over ten multiplayer modes with dozens of maps – from time proven shooter classics to fresh experiments, as well the elaborated system of ranked matches with unique rewards await its heroes.

Compelling PvE
Play with your friends in exciting online missions or try yourself in full-scale raids with several difficulty levels and epic loot.

Diverse classes
Rifleman, Medic, Engineer and Sniper are at your service: assault the defenses of your enemies with a heavy machinegun, revive your teammates, mine the corridors or take down the foes with precise shots – here you decide how to act!

Enormous customizable arsenal
Hundreds of weapons are at your disposal, customizable with a range of scopes, grips, flash guards, and camo to get the best performance tailored to your preference.

Jump into action and play for free!`,

// Victim Cache the RPG - An 80s JRPG Parody (https://store.steampowered.com/app/1115970/Victim_Cache_the_RPG__An_80s_JRPG_Parody/)
`“Sex, drugs, and leveling up!”
Victim Cache: The RPG is a parody of popular Japanese role playing games from the 1980s and 1990s, such as Final Fantasy I-VI and Dragon Warrior I-IV. Although the game play, story, and graphical style mimic RPGs of that era, Victim Cache: The RPG features mature humor, themes, and language.

The gameplay is modeled after classic JRPGs. Players can move their characters up, down, left, or right on a top down 2D map to explore the 8-bit towns, dungeons, and overworld. Along the way, players must talk to archetypal non-player characters to advance the quest lines. Battles are fought from a side view perspective in which the player can choose to attack, cast a spell, execute a special attack, use an item, or later in the game summon additional allies to help. Battles are won when all enemies hit points have reached zero. `,

// HuniePop (https://store.steampowered.com/app/339800/HuniePop/)
`HuniePop is a unique sim experience for PC, Mac and Linux. It's a gameplay first approach that's part dating sim, part puzzle game, with light RPG elements, a visual novel style of presentation, an abrasive western writing style and plenty of "plot".

After a pathetic attempt to try and pick up Kyu, a magic love fairy in disguise, she decides to take you under her wing and help you out with your crippling inability to meet new women. After a few dating lessons and some sound advice, Kyu sends you out into the world ready to take on the dating world and a wide cast of beautiful babes.

    8 gorgeous girls each with their own personalities, preferences and style. (Rumor has it there are several secret characters to unlock as well!)
    More than 20 in-game locations each with beautiful hand painted environment art and many with full day/night cycles.
    Over 250 unique items (food, drinks, gifts, accessories) that you can purchase, unlock, collect and give as gifts.
    A beautifully composed soundtrack containing over 20 tracks of original music inspired by eastern pop, jazz and bossa nova melodies.
    Thousands of lines of dialog, fully and professionally voice acted, complete with a unique character lip-syncing system.
    Customize girls' appearances with unlockable outfits and hairstyles! The girls will also dress up for dates! (Or dress down afterwards...)
    Collect nearly 50 stunning CG photos depicting the girls in various scenarios, including steamy bedroom scenes.
    An addicting match-3 style puzzle dating system with it's own unique twists including upgradeable traits/stats, the ability to move any number of positions and special date gifts that have a variety of interesting effects on the puzzle.`,

// Sakura Swim Club (https://store.steampowered.com/app/402180/Sakura_Swim_Club/)
`In Sakura Swim Club, follow the story of Kaede, just your average high school guy. Upon transferring to a new school, he meets two beautiful girls in the swim club. Things haven't gone well for the swim club lately, but with Kaede's help, that all changes. `,

// If My Heart Had Wings (https://store.steampowered.com/app/326480/If_My_Heart_Had_Wings/)
`After a bicycle accident shatters his dreams of becoming a racer, Aoi Minase returns to his hometown, depressed and defeated. With seemingly no hope left for the future and no idea what to do with himself from here on, Aoi meets Kotori Habane―a young girl stuck in a wheelchair with a flat tire. It is on that windmill-dotted hill that Aoi's somber yet touching story of youth, friendship, and flight begins.

Upon witnessing a glider soar overhead, Aoi sets off on a journey to realize the dream everyone has as a child: the dream of flight.

Working together with Aoi’s childhood friend, Ageha Himegi, the main characters of this visual novel begin rebuilding the Soaring Club, which is on the brink of disbandment at the hands of an uncaring school board. These beginners take hold of their passion, overcoming failures, obstacles, and interference from the school to put everything they have into building their own working glider.

Their ultimate goal: to fly through the legendary "Morning Glory"―a rare and stunning weather phenomenon producing a special type of roll-shaped cloud that appears in the morning when the conditions are just right.

Join Aoi on this journey as he learns about the sky, his new friends, and much more.

    Total Play Time: 20 to 30 hours
    Five character-specific story branches to play through
    Full voice-overs throughout the game for all characters`,

// CROSS†CHANNEL: Steam Edition (https://store.steampowered.com/app/812560/CROSSCHANNEL_Steam_Edition/)
`”This is the Gunjo Broadcasting Club. Is there anyone left out there...?”
CROSS†CHANNEL: Steam Edition is the first official English localization of the visual novel cult classic CROSS†CHANNEL.

Set in Gunjo Gakuin, a school specialized for those deemed by the government to be unfit for society, CROSS†CHANNEL follows the story of eight members of the school's Broadcasting Club who have just returned from a disastrous field trip, which throws their club – and their friendships – into turmoil.

Upon returning to school, the students quickly discover that they are the only living people around. Completely alone and rife with tension, will the students manage to mend their broken friendships and complete their broadcasting antenna in order to reach out to other possible survivors of whatever mysterious calamity has befallen them? Or will they come apart at the seams?
Game Features

    The definitive masterpiece of Romeo Tanaka, veteran creator of numerous popular adventure games.
    The first non-Japanese localized release of the series, based on CROSS†CHANNEL 〜For all people〜.
    The story, widely hailed as the pinnacle of "loop" tales, portrays eight young people living the same week over and over for eternity.
    Over 20 hours of gameplay, featuring multiple routes.

Story
Summer has come.
With summer comes a long vacation from school.
The Broadcasting Club has been torn apart, left to slowly disintegrate.

Since returning from their first field trip of the summer, there's been a palpable distance between the club members.
Now, only one single member continues to participate in the club activities.

But a horrible realization has dawned on all of them: their school has been completely deserted. Are they really the only people left?

The hero of our story is a member of the Broadcasting Club.
He casually greets his friends as they show up at the otherwise deserted school.

On the rooftop, we find club president Misato Miyasumi assembling a massive broadcast antenna, alone.

This antenna was the club's summer project, and its completion for the purpose of sending a broadcast out to find other possible survivors has been their main goal.

Last summer, the club comprised a close-knit group of friends who worked together. Yet now, only a single member continues their efforts.

A group of the former friends watch her work, a cold glint in their eyes.
This is the story of their clashing differences rising to a boil, and the resolutions that arise from these clashes.
Once divided, the club members slowly begin to grow closer once again... `,

// Destiny 2 (https://store.steampowered.com/app/1085660/Destiny_2/)
`When Destiny 2: Shadowkeep launches on October 1st, Destiny 2 PC will move to the Steam platform. Existing Destiny 2 PC players will be able to migrate their Guardians, gear, game progress, and previous purchases to Steam at no cost.
We are committed to making this transition as simple and seamless as possible for our existing PC community. No action required at this time, for more information, as well as future information and updates, please visit bungie.net/pcmove.
Destiny 2: Shadowkeep Pre-Order Pack
Pre-order to receive the Two-Tailed Fox Exotic Rocket Launcher and an exclusive Hive-themed Ornament.*

*Available when Destiny 2: Shadowkeep releases.
Destiny 2: Shadowkeep
Destiny 2: Shadowkeep does not require ownership of any previous Destiny 2 expansions to purchase or play.

Pre-order Destiny 2: Shadowkeep and receive Destiny 2, Curse of Osiris, and Warmind for free on October 1st.

New nightmares have emerged from the shadows of our Moon. Called forth by haunting visions, Eris Morn has returned. Join her to slay these nightmares before they reach out beyond the Moon to cast humanity back into an age of darkness.
Features
• New Missions, & Quests• New Destination & Dungeon
• New Weapons and Gear to Earn
• An All New Raid
• And More

The standalone edition of Destiny 2: Shadowkeep includes a digital copy of the game and one Season Pass, providing exclusive activities and rewards.*
Destiny 2: Shadowkeep Digital Deluxe Edition
Digital Deluxe items include
• Four Season Passes*, each offering a unique set of exclusive Seasonal activities and rewards
• Exclusive Eris Morn themed items:
    • Exotic Emote
    • Exotic Ghost Shell
    • Emblem

*Season Pass redeemed when logging in. If players already own the current Season Pass, they will receive credit toward future Seasons up until Season 11. Any credits beyond Season 11 will be exchanged for an equivalent value of Silver.`,

// Halo: The Master Chief Collection (https://store.steampowered.com/app/976730/Halo_The_Master_Chief_Collection/)
`For the first time ever, the series that changed console gaming forever comes to PC with six blockbuster games in one epic experience, delivered over time and available for individual purchase. Featuring Halo: Reach along with Halo: Combat Evolved Anniversary, Halo 2: Anniversary, Halo 3, the Halo 3: ODST Campaign and Halo 4. Now optimized for PC with mouse and keyboard support, native PC features and up to 4K UHD and HDR, this is the collection Halo fans have been waiting for.

Honoring the iconic hero and his epic story, The Master Chief Collection offers players their own exciting journey through the Halo saga. Starting with the incredible bravery of Noble Six in Halo: Reach and ending with the rise of a new enemy in Halo 4, the games will release in order of the fictional story. When complete, the Master Chief’s saga will total 68 campaign missions from all six games.

Each game released into the collection also adds new multiplayer maps, modes and game types, building a unified multiplayer suite that expands over time. When finished, The Master Chief Collection will have the most diverse and expansive Halo multiplayer experience to date, with more than 140 multiplayer maps (including the original Halo Combat Evolved maps) and Spartan Ops maps. Not to mention, countless ways to play with community created Forge content.

For the latest intel on The Master Chief Collection and all things Halo, visit http://halowaypoint.com`,

// Serious Sam Classic: The First Encounter (https://store.steampowered.com/app/41050/Serious_Sam_Classic_The_First_Encounter/)
`Note: When you purchase Revolution you get Serious Sam Classic: The First Encounter and Serious Sam Classic: The Second Encounter.

Serious Sam is a high-adrenaline arcade-action shooter heavily focused on frantic arcade-style single player action. In a world where cyberpunk meets fantasy-fiction and advanced technology is mixed with black magic and psycho-powers, Sam travels through the beautiful world of ancient Egypt and several diverse planets, confronting countless Mental's minions on his way to the Mental's base.
Key Features

    Frantic Arcade-Style Action - Fight your way through 15+ nightmarish warriors in Mental's army, including the charging Sirian Werebull, screaming Headless Kamikaze and other crazy monsters, and finally challenge the multi-story Ugh-Zan boss!
    Various Environments and Secrets - Unleash mayhem in ancient Egypt. Discover over 80 secrets hidden in the fray!
    Special Weapons and Power-Ups - Wreak havoc with a huge arsenal of weapons including Double Shotgun, Rocket Launcher, Minigun and Cannon!
    Co-Op Multiplayer - Embrace the mayhem with up to 16 players!
    Versus Multiplayer – Drop the gauntlet and let the heavy ordinance fly in incredible Deathmatch and Scorematch!
    Split Screen – Huddle up with up to 4-player local split screen co-op and versus modes!
    Level Editor – Create your own levels, MODs, textures and other content with the inclusion of the fully featured Serious Editor and Serious Modeller!`,

// DOOM (https://store.steampowered.com/app/379720/DOOM/)
`Developed by id software, the studio that pioneered the first-person shooter genre and created multiplayer Deathmatch, DOOM returns as a brutally fun and challenging modern-day shooter experience. Relentless demons, impossibly destructive guns, and fast, fluid movement provide the foundation for intense, first-person combat – whether you’re obliterating demon hordes through the depths of Hell in the single-player campaign, or competing against your friends in numerous multiplayer modes. Expand your gameplay experience using DOOM SnapMap game editor to easily create, play, and share your content with the world.

STORY:
You’ve come here for a reason. The Union Aerospace Corporation’s massive research facility on Mars is overwhelmed by fierce and powerful demons, and only one person stands between their world and ours. As the lone DOOM Marine, you’ve been activated to do one thing – kill them all.
KEY FEATURES:

    A Relentless Campaign
    There is no taking cover or stopping to regenerate health as you beat back Hell’s raging demon hordes. Combine your arsenal of futuristic and iconic guns, upgrades, movement and an advanced melee system to knock-down, slash, stomp, crush, and blow apart demons in creative and violent ways.

    Return of id Multiplayer
    Dominate your opponents in DOOM’s signature, fast-paced arena-style combat. In both classic and all-new game modes, annihilate your enemies utilizing your personal blend of skill, powerful weapons, vertical movement, and unique power-ups that allow you to play as a demon.

    Endless Possibilities
    DOOM SnapMap – a powerful, but easy-to-use game and level editor – allows for limitless gameplay experiences on every platform. Without any previous experience or special expertise, any player can quickly and easily snap together and visually customize maps, add pre-defined or completely custom gameplay, and even edit game logic to create new modes. Instantly play your creation, share it with a friend, or make it available to players around the world – all in-game with the push of a button.`,

// Duke Nukem Forever (https://store.steampowered.com/app/57900/Duke_Nukem_Forever/)
`The King is Back!

Cocked, loaded and ready for action, Duke delivers epic ass-kicking, massive weapons, giant explosions and pure unadulterated fun!

Put on your shades and step into the boots of Duke Nukem. The alien hordes are invading and only Duke can save the world. Pig cops, alien shrink rays and enormous alien bosses can’t stop this epic hero from accomplishing his goal: to save the world, save the babes and to be a bad-ass while doing it.

The King arrives with an arsenal of over-the-top weapons, non-stop action, and unprecedented levels of interactivity. With hours and hours of action, and a range of bodacious multiplayer modes, rest assured knowing the fun goes on and on.

Story
Did the Alien bastards not learn their lesson the first time? Duke has been on hiatus for some time now, kicking back and franchising himself on the fame he gained from saving Earth from the first invasion. The Aliens have returned to Earth yet again, messing up Duke’s sweet routine of dirty leisure habits.
The Alien invaders are stealing Earth’s women, especially the hot ones! And they drank Duke’s beer. This. Won’t. Stand. As Duke battles his way through waves of aliens, the once beautiful gambling haven and Duke Nukem franchise chains are crumbling before his eyes. Time to bring the pain!

Features

    Bust a Gut: Duke pulls no punches. Duke’s constant stream of hilarious one-liners throughout the game make this an out loud good time.
    World Interactivity: Spend as much time as you want shooting hoops, lifting weights, playing pinball, pool, air hockey, and slots.
    Scale & Variety: Packed with explosive FPS action, outlandish settings, driving, and puzzle solving — gamers will never tire of the endless FUN.
    Multiplayer Like No Other: Classic modes are re-made with a Duke twist. Shrink, squash, freeze and shatter your opponents, or just frag them with a rocket.`,

// Unreal Gold (https://store.steampowered.com/app/13250/Unreal_Gold/)
`Your prison ship has crash-landed on the fastest, sleekest, most dangerous 3D world ever created. Look around, crystal clear water shimmers, shadows dance and shift, alien architecture fades into the horizon. Discover the secret of this mysterious planet and find out what caused a peaceful race to be enslaved by vicious merciless aggressors.

    Over 47 eye-popping single player missions.
    20 multiplayer levels with 5 different game types.
    Ruthlessly intelligent enemies, each with unique personalities.
    An arsenal of over 13 deadly weapons...all guaranteed giblet action.
    Includes all of the latest Unreal enhancements and technical improvements.
    Special graphical and performance enhancements for the latest 3D cards.

© 1998-1999, Epic Games, Inc. All rights reserved. Epic, Epic Games, Unreal, and Unreal (Stylized) are trademarks or registered trademarks of Epic Games, Inc. in the United States of America and elsewhere.`,

// Sniper Fury (https://store.steampowered.com/app/591740/Sniper_Fury/)
`INITIATING GLOBAL OPERATIONS:
The time for diplomacy is gone. We are calling for the best sniper in the world to join us as we take aim at evil, wherever it hides. This is not a game. There is no room for remorse here, so shoot to kill.

A STUNNING FIGHT AGAINST EVIL
• 130+ missions.
• Soldiers, armoured vehicles, air units and many more enemy classes.
• Next-gen “bullet time” effects capture your every amazing sniper headshot.
• Sandstorms, blizzards, rainstorms and other rich atmospheric effects.

MODERN FIREPOWER
• Shoot sniper rifles, assault rifles, railguns and top-secret weapons
• Gather components to upgrade your military arsenal
• Unique weapon personalization

PVP MULTIPLAYER CHALLENGE
• Steal resources from other players by breaking down their defenses in PvP Multiplayer mode
• Build a strong squad to keep your loot safe in PvP Multiplayer mode

EASY TO PLAY, MUCH TO MASTER
• Varied AI behavior makes each enemy unique and challenging to shoot & kill
• Extra rewards for joining the action in special events
• Connect to the game community to discover more content, more contests and more rewards!

LEAGUES
• Earn a high enough score to enter a league and unlock rewards! Then check out who's top of your league on the Leaderboards. Guard your gold and secure your position using the brand-new SHIELD defence system.

CLANS UP FOR GRABS
• Join or create sniper Clans to team up with other players.
• Trade and share resources with your Clan mates to strengthen each other.
• The group Chat lets you talk strategy and socialise with your allies.

CLAN HQ
• Establish a home base for your Clan that members can help improve together. Clan HQ buildings can unlock loads of perks, including PvP boosts, Tier-5 Squadmates and much more.

CLAN WARS
• Prepare for a new game mode where Clans go head-to-head in the ultimate showdown. Declare war, weaken the opposing clan's Reactor Core, and pillage resources from them! `,

// Aim Lab (https://store.steampowered.com/app/714010/Aim_Lab/)
`Aim Lab is still very very early in development (about 35% complete), and as such, lack of polish, and game-breaking bugs are to be expected. Support for languages other than English is incomplete. Please do not download if you are looking for a finished version, and instead add Aim Lab to your wishlist, join us on Discord, and follow along with the devlog updates!

“The best aim trainer on the market easily highly recommend this.”
- Mr Bombastic (Steam Review)
“Best aim trainer I have tried so far, can definitely feel myself improving after even the first handful of games and the data and visualizations are super helpful.”
- Toast (Steam Review)
“Very usefull, simple to configure, well done !”
- Riton de Lyon (Steam Review)
“If you have a program like this it will save you hundreds of hours”
- Tommy “Potti” Ingemarsson, 10 time Counter Strike world champion. (Motherboard article: “These Neuroscientists Think They Can Get You More Headshots”)
“What a fascinating mesh of technologies this brings to the scene. If enough players embrace Aim Lab, Statespace could really shake up the esports scouting dynamic.”
- Jay Masaad, Esports Insider (Esports Insider article: “Statespace Brings Neuroscience to Esports”)

DETAILED FEEDBACK AND STATS
We identify your weaknesses and then help you eliminate them. Each training task has its own unique analytics focused on a particular area, while your data profile allows you to measure and track performance improvements over time.

EASY TO SET UP AND MAP GAME SETTINGS
Pick your language, pick the game settings you want to use, check your graphics settings and that’s it! We currently support game setting conversion from:
• Apex Legends		　• Battalion 1944		　• CS:GO
• Call of Duty: Black Ops 4		　• Fortnite		　• Overwatch
• PUBG		　• Paladins		　• Paladins
• Rainbow 6 Siege		　• Reflex Arena		　• Realm Royale
• Ring of Elysium		　• Team Fortress 2		　• Warface

GO BEYOND AIM TRAINING
The fundamental skills necessary to be a great FPS player go beyond just aiming, and Aim Lab tests and trains those skills as well: visual detection speed, attention capacity, short term memory, audio-spatial skills, and decision-making.

CUSTOMIZE (ALMOST) EVERYTHING, even with controller!
Create and customize your crosshair settings, target colors, edit and save custom tasks! Steam Workshop integration coming soon, along with customizable weapons and even maps!

LOTS OF WEAPONS
Pistols? Got ‘em. Shotguns? Got ‘em. Assault Rifles? Sniper Rifles? Rocket Launchers? Yes, yes, and yes. Hitscan weapons, projectile weapons, and even orbs that shoot from your hands. `,

// 中国式家长 / Chinese Parents (https://store.steampowered.com/app/736190/__Chinese_Parents/)
`In this casual yet realistic life sim with a Chinese authenticity, you step into the shoes of an average kid from the first day of life towards the end of your high school days.

Study hard, have fun, make friends and face the “Gaokao”, one of the most critical examinations in your life. And perhaps this experience might also give you a special perspective to explore your relationships as a parent and as a child.

A unique slice of life simulator
You are a child born in an ordinary Chinese family. Prior to Gaokao, one of the most critical examinations, you have 18 years to enjoy your life and make choices to be yourself.
Be a Chinese kid
As a girl or a boy, different experiences from heartfully weaved stories and distinctive characters awaits you.
Use “Fragments” to improve yourself
Raise your stats with Fragment mini-game. Higher stats improve your character through learning new skills.
Tiger Parent, or not?
Scheduling every detail, either ensure a happy and stress-free life, or force the kid to study as hard as possible? It’s all up to you.
Live the life as a Chinese child with various mini-games
Expect a life full of challenges. Do you have what it takes to defeat your annoying and cocky neighbors in the Face Duel? Or can you maintain your cool while trying everything to grasp the Red Pocket against your relatives in a courtesy manner?
Multiple friends to date with
You are not alone, after all you need a social life. Choose and date with 14 friends in total with various interaction in order to know them better. Who knows what post-graduation fairy tales will happen to these childhood sweethearts?
Over 100 career endings
Persuade your dream career, and choose your own life. Will you settle to be an average nobody, or only after reaching the top of everything will your ambition be properly answered?
Your family journeys on, from generation to generation
Your child in the next game will benefit from your achievements in the previous generation. Don’t forget to check back your roots to find courage and wits for the future. `,

// Cute Girls 可爱的女孩 ()
`Girls invite you to their world, test your speed and earn points by pressing the keys at the right moment. Unlock new stages by scoring certain amounts of points. If you just want to watch beautiful dancing girls enter the free hands mode and relax.

Don’t forget to discover the private mode, which allows you to choose one Girl, her outfit and the scene - have a good time watching her moves in dance!
In private mode you have free camera - you can move the camera wherever you’d like and enjoy the girls show from any distance or angle.

Features:

- Challenge mode
- Free hands mode
- Private mode
- Different stages
- Unique music
- High quality anime models
- Diverse dance styles`,

// Anime show 动漫时装秀 (https://store.steampowered.com/app/809840/Anime_show/)
`Twelve sexy girls compete with each other for the title of Miss Anime VR.
Take part in the show as jury and decide which girl’s dream will become
true and choose the winner of anime fashion show.

Become a jury in an entertainment show!

It is up to you what will happen with twelve sexy girls.
Will their dreams become true? Don’t keep them waiting!
Put the VR headset on and prepare yourself for unforgettable experience:
twelve beautiful girls will compete on three specially designed,
unique stages for the title of Miss Anime Fashion.
On each stage girls will present themselves both in directed,
joint performance and individually. Sit back and let yourself be pulled in.`,

// VR Kanojo / VRカノジョ (https://store.steampowered.com/app/751440/VR_Kanojo__VR/)
`This is Steam version.

"all characters in this game are 18+"

We have a trial version here.
Looking for a unique VR experience? Well, look no further!
In VR Kanojo, you can hang out with the lovely girl-next-door, Sakura Yuuhi. You'll practically feel her breath on your cheek and the warmth of her fingers on your arm as you laugh and talk the day away. Better yet, VR controllers simulate your hands in-game, letting you interact with her more directly. Just imagine all the possibilities!

Oh yeah! And you can customize Sakura's appearance with a variety of different outfits too. Sound like fun? You bet it is!

So, thanks for buying VR Kanojo, and we hope you enjoy your time with Sakura Yuuhi!

[How to change Language]`,

// Dead by Daylight (https://store.steampowered.com/app/381210/Dead_by_Daylight/)
`Death Is Not an Escape.

Dead by Daylight is a multiplayer (4vs1) horror game where one player takes on the role of the savage Killer, and the other four players play as Survivors, trying to escape the Killer and avoid being caught, tortured and killed.

Survivors play in third-person and have the advantage of better situational awareness. The Killer plays in first-person and is more focused on their prey.

The Survivors' goal in each encounter is to escape the Killing Ground without getting caught by the Killer - something that sounds easier than it is, especially when the environment changes every time you play.

More information about the game is available at http://www.deadbydaylight.com
Key Features
• Survive Together… Or Not - Survivors can either cooperate with the others or be selfish. Your chance of survival will vary depending on whether you work together as a team or if you go at it alone. Will you be able to outwit the Killer and escape their Killing Ground?

• Where Am I? - Each level is procedurally generated, so you’ll never know what to expect. Random spawn points mean you will never feel safe as the world and its danger change every time you play.

• A Feast for Killers - Dead by Daylight draws from all corners of the horror world. As a Killer you can play as anything from a powerful Slasher to terrifying paranormal entities. Familiarize yourself with your Killing Grounds and master each Killer’s unique power to be able to hunt, catch and sacrifice your victims.

• Deeper and Deeper - Each Killer and Survivor has their own deep progression system and plenty of unlockables that can be customized to fit your own personal strategy. Experience, skills and understanding of the environment are key to being able to hunt or outwit the Killer.

• Real People, Real Fear - The procedural levels and real human reactions to pure horror makes each game session an unexpected scenario. You will never be able to tell how it’s going to turn out. Ambience, music, and chilling environments combine into a terrifying experience. With enough time, you might even discover what’s hiding in the fog.`,

// Chef: A Restaurant Tycoon Game (https://store.steampowered.com/app/886900/Chef_A_Restaurant_Tycoon_Game/)
`Chef allows you to personally don a kitchen apron and embark on an exciting career in the wild world of professional cooking. Starting with nothing but pocket change, a small restaurant, and an ambitious dream, you’ll ascend from the life of a humble cook to that of a world-famous chef.

Nothing will prevent you from deciding how to climb to the top of the food chain. Prepare yourself for a true sandbox experience in which all your management choices matter as you build your gastronomic empire. What will it be? A steakhouse? A vegan paradise? A pasta palace? An experimental cooking lab? Only you can figure out which path is best for your establishment.
FEATURES LIST

    Create your own avatar and level them up across 6 different skill trees with more than 100 abilities to choose from.
    Full restaurant management: Create the restaurant’s location and layout, hire the staff, design the menu, and determine the restaurant’s policies.
    Customize every aspect of your restaurants: Choose from over 150 different variations for floors and walls, and over 200 appliances and decorations that can be individually placed and colored.
    Experiment with a realistic recipe editor: Use the skills and ingredients at your disposal to create one-of-a-kind dishes that are accurately rated by taste and aroma.
    Choose your own cooking style: Specialize in meat or seafood, go vegetarian or vegan, or offer a vast selection of spicy or exotic dishes. The choice is yours and the world will react to it accordingly.
    Hundreds of events and minor storylines will weave an emergent narrative around your choices and actions.
    Extended modding support: Getting tired of the game after playing it for two hundred hours? Thanks to Chef’s mod support, you will never run out of fresh content to enjoy!`,

// CoffeeBiz Tycoon (https://store.steampowered.com/app/674370/CoffeeBiz_Tycoon/)
`CoffeeBiz tycoon is a mix of entrepreneurship fun and complicated business simulation. Start your first coffee kiosk, brew, expand, hire, deal with competitors, build a well-known brand, risk, and do everything else needed to build a prosperous and profitable business. Created in isometric pixel art graphic style.

This game is not another ‘click and wait’ kind of game. No any kind of loot boxes or ‘pay for content’ either. Its deep and detailed business simulation game with ‘hard to win, easy to get bankrupt’ concept in its core.

Released Content & Features:

    Pixel-perfect 2.5D graphics with dynamic lighting
    Two game modes: sandbox and scenarios.
    City to explore and decide where is the best location to run business
    Kiosk to brew coffee and sell it to customers. Buy ingredients, place coffee machine and barista, set prices, promote and make money.
    Four customer types with different needs and demand to analyze.
    Employees to hire, fire, motivate and educate. Assign them to the barista, assistant or managers positions. Watch for their energy and mood.
    Equipment (like coffee machines) to research, buy, service and repair to keep coffee quality and brew speed at high levels
    Random events like employee illness, vandalism, and other that will affect your operations and sales
    Office with the manager to organize and improve business operations and research new business opportunities
    Detailed finance simulation
    Unlimited amount of business strategies to try out`,

// Bitcoin Trading Master: Simulator (https://store.steampowered.com/app/848310/Bitcoin_Trading_Master_Simulator/)
`A new feature: Quantitative Strategy Beta

About yourself:

Do you want to learn how to trade digital currencies such as Bitcoin, ETH, EOS, and more?
Do you ever find that the online exchange is so difficult, for it requires complicated procedures such as registration, certification, and saving coins?
Do you want to buy a few coins as a trial, only to find that you can't afford it, because all exchanges have set a minimum transaction amount?
Do you have the confidence to make a fortune in online trading?

Anyway, you may have a variety of reasons for not having tried before. But now you can learn these skills through this game. If you are a veteran already and have made a couple of transactions on an online exchange, you can learn in this game additional advanced techniques all the same, like leveraged trading, futures, and so on. If you take no interest in digital currencies, it does not matter. Who knows if this will not be useful in the future? It is always right to have a receptive mind.

About game:

This is a simulation of the digital currency trading game, the data is synchronized in real-time from online exchanges (such as: GDAX, BINANCE).

You can learn how to trade digital currency in this game. The game currently supports more than 100 kinds of digital currency exchanges and equip with time-sharing trading charts to help you grasp market conditions.

The digital currency price changes every minute as the market fluctuations, therefore you need to grasp market conditions accurately. You can do short-term operation buying or selling quickly. And you can also do long-term operation buying a currency and selling it at the right time, depending on your strategy.

The leaderboard feature has added to the game. According to your profit, there will be a ranking of global players. It's high time to show your talent, what are you waiting for? `,

// Parkitect (https://store.steampowered.com/app/453090/Parkitect/)
`Welcome to Parkitect, where you build and manage the theme parks of your dreams.
Construct your own coasters, design an efficiently operating park that fully immerses your guests in its theming and play through the campaign.
Build

Create the perfect park for your guests! Deform the terrain, place water, build structures! With a huge selection of deco objects from various themes you can design your park however you want. And for more you can get custom scenery from the Steam Workshop or mod in your own!

Top off that park with your very own roller coaster! Design it yourself or choose from a number of exciting designs. Spinning, looping, launching, flying - with over 70 of the worlds most popular types of theme park rides you'll always be able to surprise your guests.
Manage

Building is only half of the challenge! You'll need to keep an eye on the finances and guest satisfaction to stay on top, and for the first time in a theme park game the "behind the scenes" parts of your park have a meaning too! Route resources to your various shops without annoying your guests and keep the staff areas out of the public eye for the perfect immersive experience.
Play

Build your way through a challenging campaign! 26 scenarios with unique settings will put your theme park management and coaster designing skills to the test. And there's always more - download scenarios created by the community from Steam Workshop or design your own with the scenario editor. With the landscape generator you'll create your own unique setting in no time. And if you're more of a creative player there's the sandbox mode! `,

// Software Inc. (https://store.steampowered.com/app/362620/Software_Inc/)
`Construct and design buildings for optimal working conditions. Hire people to design and release software, so you can defeat the simulated competition and take over their businesses. Manage and educate your employees to make sure they are skilled and satisfied with their job.

CURRENT FEATURES

    Build, furnish and maintain office buildings up to ten stories + basement, on an enormous land, using a free-form building system with easy copy-paste tools
    Hire employees to design, develop, support, research and market software in teams
    Build roads and parking to ease commuting for your employees
    Tend to your employees' needs, demands, skills and specializations, while making sure each team has compatible personalities
    Customize your own employee avatar
    Create your own software products and franchises
    Compete in a simulated and randomly populated market by selling your products, taking on contract work, creating patents, making deals or trading stocks
    Hire staff to repair your furniture and computers, clean your office, make food for your employees or greet visitors
    Mod what kind of software you can develop, add your own furniture, upload your building blueprints or add support for your language
    Delegate important tasks to your team leaders, such as managing development cycles and human resources
    Set up your own servers for products, source control and running your own online store`,

// Crypto Crisis (https://store.steampowered.com/app/889720/Crypto_Crisis/)
`Where were you during the rise of Bitcoin? Crypto Crisis is a simulation game which drops you into the beginnings of Bitcoin. It is early 2009, after getting in trouble with some shady characters you decide to start Bitcoin mining to turn your life around. Compete for and increase your share of block rewards by building and upgrading your mining rigs while carefully managing your resources.

Released Content & Features:

    Relive Bitcoin history with a closely simulated Bitcoin network which utilises historical data
    Compete for and increase your share of block rewards by building, upgrading, optimising and overclocking your mining rigs
    Carefully manage the energy consumption and heat output of your mining rigs; the more energy you use the higher your electricity bill will be
    Sell now or HODL? Earn money by selling your Bitcoin on the market
    As you progress in your mining career move to new maps to unlock additional space and resources
    Keep upgrading your mining rigs with periodic releases of new chassis, CPUs, GPUs and ASICs
    Buy and upgrade utility equipment to help you manage energy consumption and heat output
    Mining and network statistics help you keep a track of your progress and how well you are performing versus your competitors
    Financial breakdown statistics help you determine where all of your resources are being spent
    4 navigable 3D maps which update as you build and upgrade your mining rigs
    20+ chassis types
    15+ utility equipment types
    170+ CPU/GPU/ASIC parts`,

// Seeds of Resilience (https://store.steampowered.com/app/877080/Seeds_of_Resilience/)
`Every month an update
We've been adding new content and bug fixes every month. We only have 1 developer on it so it takes time, but we commit on adding new things monthly until release! Check out the news steam of Seeds of Resilience for proof of this!
Build a new village from scratch
Build a village on a deserted island, and prepare for merciless natural disasters! Learn to choose the right items, understand nature's patterns, use real medieval construction and craft techniques in this turn-based management game.

Features

    Turn based: Take all the time you need to plan your actions. When you're done, click the end turn button and start a new day.
    Detailed building construction: Choose natural resources according to their properties. Use them to craft the materials needed to assemble a building.
    Realistic medieval construction and craft techniques. Everything could be made in real life the same way.
    Observe the environment response to human activity. Maybe you should avoid fishing everyday at the same spot or cut down the whole forest.
    Survive in a harsh environment where storms and other natural disasters occur way too often.

Harvest, craft, build
Build your civilization step-by-step: From stone axes and stick shelters to waterwheel powered mechanized workshops! All with realistic technologies and constructions.
Inspired by real medieval techniques
We made extensive research about medieval construction and craftsmanship, as well as survival techniques. We simplified and balanced data to fit the game’s needs, while remaining consistent with how things work in real life.


To learn more, read our survival guides illustrated with the work of archaeological designer Francesco Corni. `,

// Sigma Theory: Global Cold War (https://store.steampowered.com/app/716640/Sigma_Theory_Global_Cold_War/)
`STORY
From the creators of the award-winning sci-fi game Out There...
In the near future, a paradigm-shifting scientific discovery looms over the world, promising radical new technologies. The world’s superpowers realize they could have the power to destroy the global financial system, wipe out entire countries or even gain access to immortality.

However, this discovery — called “The Sigma Theory” — can only be harnessed by a handful of scientists. You are placed at the head of your country’s Sigma division. Your objective is to ensure that it is your nation that reaps the benefits of Sigma Theory before anyone else.

To achieve this you will have powerful resources at your disposal: a cadre of the world’s most elite covert agents, advanced tactical drones and, of course, your own skills in diplomacy and subterfuge.

It’s a cold war out there, one in which mankind must face up to its future.

THE ULTIMATE ESPIONAGE SIMULATION
Turn-based espionage: Use your special agents to dominate the world. Seduction, blackmail, manipulation, industrial espionage… Every low blow is both permitted and encouraged.
Dynamic narrative: Develop and manage your relations with over 100 NPCs: lobbies, armed groups, politicians… Alliance, deception or assassination, you choose.
Field operations: Direct the kidnapping of your targets during gripping pursuits through the world’s greatest cities. Discretion or direct confrontation, your agent’s life is in your hands.

Recruitment
First, recruit a team of four special agents from 50 unique unlockable characters to support your missions. Each comes with their own story, motivations and traits that define their behavior during missions.
Access the investigator, soldier, hacker, seducer and other archetypes to complete your Sigma division and execute your strategy.

Espionage
When your team is ready, assign them missions around the world to gather scientists, discover compromising documents on local politicians, and more.
Exploit your agents’ abilities to seduce, manipulate and bribe your targets over to your side or have them neutralized.

Counter-espionage
Other nations will also use their agents to infiltrate your country and seize your scientists. Track, capture and interrogate them to gain information on your enemies, then exchange them for ransom when they are no longer useful.

Exfiltration
Order your agents to exfiltrate scientists recruited to your cause during gripping turn-based exfiltration phases through the world’s greatest capitals. Choose the position your drones wisely to support these delicate missions.

Diplomacy
In Sigma Theory, diplomacy is the key to achieving your goals. Meet your foreign counterparts and further your objectives using flattery, threats or blackmail. You can also recover your captured agents, advance your research, and much more. Develop your relations with powerful lobbies and armed and influential groups to obtain their favor. But be careful, they may turn on you.

Tech Tree
Sigma Theory is the cause of this cold war. Rally scientists from around the world to your cause to discover new technologies to gain the edge and change the world. Mind control, destabilization of the world economy, robot soldiers, immortality… Will you keep these discoveries for yourself or share them with the world?
Games that inspired us:
XCom, Phantom Doctrine, Plague Inc, Pandemic (board game), Rocket Ranger, Tropico, Armello, Civilization, curious expedition and so much more `,

//
``,

// Hob (https://store.steampowered.com/app/404680/Hob/)
`From the team that brought you Torchlight and Torchlight II comes Hob: a vibrant, suspenseful action-adventure game set on a stunning and brutal world in disarray. As players delve into the mysteries around them, they discover a planet in peril. Can it be mended, or will the world fall further into chaos?

Features
WORDLESS NARRATIVE: Presented without text or dialogue, Hob’s story is revealed as you explore the planet and interact with the strange lifeforms that inhabit it.

TRANSFORM THE WORLD by solving puzzles and repairing the planet. The landscape will change before your eyes, opening new areas to uncover and explore.

GRAPPLE, PUNCH, and WARP through the world! Use your mechanical glove-arm abilities for traversal as well as combat.

ADVENTURE in an open world, explore ruins, befriend sprites, and battle the rogue creatures that threaten their extinction. `,

// Ancient knowledge (https://store.steampowered.com/app/1101770/Ancient_knowledge/)
`Ancient knowledge is a story in which you will explore the ruins of settlements of the powerful ancient civilization that lived here long time ago, overcoming the traps and puzzles they left.

Main features:

- Nice low poly graphics and attention to detail

- Magical atmosphere that will allow you to immerse yourself in the culture of ancient India

- Explore ancient palaces and temples, overcoming many traps

- A large number of different types of puzzles `,

// Hiiro (https://store.steampowered.com/app/464960/Hiiro/)
`Hiiro is a 2D platform game focused on ambient exploration and puzzle solving. Discover a grand world filled with mysterious artifacts and forgotten ruins. Become immersed in relaxing gameplay and meditative music. Remain observant as you unravel an explanation for your solitude. In-game death not included.

Features:
• Exploration - One vast world to travel through with eight distinct environments. Think Seiklus, Knytt, or An Untitled Story.
• Discovery - Hidden secrets and puzzles scattered throughout the world. Various levels of hidden and various levels of difficulty.
• Dynamic Soundtrack - Meditative music that seamlessly transitions as you move through the world.
• No Death - Explore a world without fear.
• No Language - A story told without words, spoken or written.
• Wide Range of Gameplay - Easy mechanics for the non-gamer, hidden secrets for the casual gamer, challenging puzzles for the hardcore gamer, awesome rewards for the completionist. Something for everyone. `,

// ATLAS (https://store.steampowered.com/app/834910/ATLAS/)
`From the creators of ARK: Survival Evolved comes ATLAS - a massively multiplayer first-and-third-person fantasy pirate adventure, now also supporting offline singleplayer and non-dedicated private session modes on the same huge world! ATLAS hosts up to 40,000 players exploring the same Globe simultaneously, with an unprecedented scale of cooperation and conflict! Stake your claim in this endless open world as you conquer territory, construct ships, search for buried treasure, assemble forts, plunder settlements and hire crew to join your powerful growing armada. Start small then expand your spheres of influence from a small island, up to an unstoppable pirate empire that spans across the oceans. Wage battle against enemy fleets as you singlehandedly command large ships of war using the captaining system (or divide up to the responsibilities among your trusted lieutenants), or take control of any weapon directly with your own character. Dive deep into the briny water to explore permanent sunken wrecks and recover salvage, unearth the loot from procedurally-generated Treasure Maps and challenge zones, or complete challenging main questlines. Team up with other aspiring adventurers and sail into the vast ocean to discover new lands rich with region-specific elements, tame exotic natural and mythical creatures, raid forgotten tombs, confront powerful ancient gods and even build and administer your own colonies, cities, and civilizations to dominate the ATLAS in this ultimate quest for fortune and glory!

MMO On the Grandest Scale, Plus Singleplayer and Non-Dedicated Private Sessions!

Physically sail in real-time across the vast oceans with the proprietary server network technology, also playable in singleplayer offline mode, as well as player-hosted non-dedicated multiplayer session modes. Explorers will voyage to over 700 unique landmasses across 45,000 square kilometers, with thousands of Discovery Zones, and ten distinct world regions each having their own unique resources, creatures, secrets, and environment hazards! There is a separate PvE ATLAS for players who don't wish to play any PVP.

Construct the Ship of Your Dreams, Plank by Plank!

Build drydocks and start with a dinghy rowboat, basic raft, tiny sloop or nimble schooner, moving on up to your own versatile brigantine or titanic galleon capable of transporting hundreds of crew and extensive cargo. Name your ship in big bold letters, paint and copy your own pirate flag and custom-place all the pieces of your ship -- which sails and where, planks and gunports, every single structure piece has a physical Weight and Material -- to function exactly how you want.

Captain your Crew!

Recruit real players and AI crew from freeports or rescue seasoned crew from destroyed “Army of the Damned” shipwrecks to man distinct stations on your boat! Set sail and explore the ATLAS with them to gain experience and level up their stats or gear, as well as levelling your Ship. Your crew are versatile! Whether on land, or ship, or even riding behind your animal companions on emplacements, they can man weapons of all kinds-- cannons, swivel guns, siege engines, turrets, and even gigantic mortars, many with dynamically swappable ammo types including grapeshot, chainshot, spikeshot, liquid fire, and more! Instruct them to board enemy ships and help you conquer the seas! Keep their stomachs full and their pockets full of gold lest you want a mutiny in your hands! Take on the Captain’s Wheel or divide up the command responsibilities via Lieutenant Podiums to direct the Ship’s assignable weapon systems, sails, and stations, including standing-orders or manual grouped fire. Or, walk up to any station and take control of it yourself!

Pirate PVP to the Limit

Everything is up for grabs including player’s ships, their crew, their pets, their forts, their land, and their booty. If you can get your hands on it, you can take it. A ship’s permanent logbook tells the tale of their legendary travel, exploits and ownership.

Be the Hero or Villain you were meant to be

MMO-scale character progression systems include, at launch, over 15 Disciplines with over 300 Skills, in a vast unlockable tree. New "Feat" system allows for active and passive bindable character abilities, while new stat-buff system allows innumerable abstract statistics to be modified by Skills, Items, and Armor. And now, everything has scalable stats, including structures, and can be progressively Upgraded!

This is Who You Are

Extensive character visual customization enables an endless range of realistic (and not-so-realistic) characters, with a vast array of sliders, morphs, muscle tones, and tweakable values. You can even per-pixel design your own permanent tattoos (and then draw warpaint on top of that too!). If someone exists in the real world, they should be creatable in ATLAS! Best-in-class dynamic hair growth and real-time aging systems allow you to get old and... pass on your legacy? (Or find a fountain of youth!).

Endless Adventure

Full Quest and Waypoint systems, with sub-quests and rewards, for major goals -- while procedural treasure maps and challenge zones also ensure there's always something new to find out over the horizon.

Intense Action

Tactical melee combat systems with blocks, parries, dodges, character motion, optional shields, stunning and strength-varying strikes, directional attacks, and more for use in either first or third person perspectives. Fists, swords, maces, blackjacks, daggers, and much more give you tremendous options to pick the right weapon for the job. Meanwhile, period-appropriate-weaponry with skill-based active reload systems include flintlock pistols, muskets, blunderbuss, and more -- just be sure to keep your powder dry!

Claim What's Yours

Form a company, claim territory and apply taxation and behavior rules to that which you own: be a benevolent governor, a feudal lord, or a ruthless dictator. Territory ownership is visualized on your dynamic zoomable World Map, with Fog of War obscuring uncharted regions and Shroud of War hiding enemy territories out of sight range. Contest other government's ownership of land, structures, or ships to expand your empire! The top large-scale Spheres of Influence on the Official ATLAS' are visualized in real-time on the PlayATLAS homepage. Design your own custom flag per-pixel to apply to your claims, to be famous (or infamous)!

Creatures Great and Small

More than 50 creatures at launch varying from breedable utilitarian farm animals & shoulder mounted parrots and monkeys that offer unique buffs, all the way to magical mermaids and gigantic sea monsters of legend. Creatures can be found in their logical regions, but take skill and tactics to tame using new mechanics, while also gaining the most benefit and fertility from their natural biomes. Ferry these temperamental creatures across the vast oceans in a virtual Noah's Ark to trade in exotic locales.

Freeports to Meet & Greet

Level-capped starter zones allow you to learn the ropes and meet new players in a safe space before you venture out into the great unknown. Where the wind and destiny takes you beyond that, is up to the gods!

Build Your Empire

Overhauled building systems include automatic foundational elevation adjustment, dynamic tile type swapping, improved snap detection & previewing, integrated plumbing systems, per-pixel-paintable everything, beast-of-burden harness attachments for field-gun and carriages, and much more! Survival systems newly include, among other aspects, Vitamins benefits and deficiencies, with recipe & cooking skills and varying ingredient qualities.

Powerful Mod and Server Functionalities

Want to build a World War II Spitfire? Or a Flying Fortress bomber with fully walkable interior and gun stations, carrying troops loaded with machine guns and rocket launchers? How about a Tank? An Aircraft Carrier to play out the Battle of Midway on an expansive scale? How about an Arcadian Steampunk Airship floating through a cloud-world? These examples and much more are provided with the ATLAS Dev Kit, where you can make effectively create whatever large-scale MMO action game you want to see happen, all supported by the database-driven network technology that powers ATLAS. Unofficial ATLAS' can be of any size and configuration, while a visual map tool lets server hosts layout their own complete custom world of islands, continents, terrain features, spawns, resources, hazards, underwater zones, dynamic weather, biome configurations, ecology, and an infinite number of other configurable features -- all dynamically streamed to the client during gameplay. The possibilities are endless!

Far, Far More

Stay tuned for more details on the extensive features and content of ATLAS. This brief sampling is just the tip of the iceberg (though be sure to watch out for those in the polar regions, as they're extremely dangerous to ship hulls!!).

A Brief History of ATLAS

Long-ago, far above the watery world of ATLAS there once existed a magic-powered “Golden Age”-- where great Empires of the Sky lived at peace alongside magical creatures, flourishing upon floating continents above the clouds, all powered by a magical source of energy known as the Heart of the Goddess, granted by the powerful beings who dwell in the stars beyond. However, a powerful warlord known as Xevos sought to unite the human civilizations over the magickind, and launched a war that broke the Heart into pieces, unleashing a wave of destruction that shattered the floating landmasses and plunged them into the waters below.

Many generations later, as living memory of the “Sundering” has faded into legend and the once-plentiful magical energy has long since dissipated, descendants of the protectors of the Heart of the Goddess -- known as “Pathfinders” -- emerge from their provincial oceanic lives to explore the new continents and islands, and seek out the power they may yet contain. Across the vast new world they’ll encounter hostile creatures, technological remnants of the Golden Age past, and ultimately the undying spirit of Xevos himself and his soulless “Army of the Damned”, still intent on recovering the pieces of the Heart of the Goddess to raise the continents once again under his vision.

If the latest generation of Pathfinders can outwit and outgun Xevos and his legions, and recover the Heart pieces for themselves, they will be faced with the very same choice: whether to attempt to restore the old world, or build a new better one… `,

//
``,

//
``,

//
``
]

export default data
