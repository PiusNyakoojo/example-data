# 🦕 example-data

![npm version](https://badge.fury.io/js/example-data.svg)
![downloads](https://img.shields.io/npm/dt/example-data.svg)
![coverage](https://gitlab.com/piusnyakoojo/example-data/badges/master/coverage.svg)
![pipeline](https://gitlab.com/piusnyakoojo/example-data/badges/master/pipeline.svg)

[Demo](https://piusnyakoojo.gitlab.io/example-data/) | [Video Tutorial](https://google.com)

`example-data` is a javascript library for generating json-formatted data that relates to examples of commonly used data in application development. Examples of commonly used application data includes `users`, `videos`, `images`, `games`, `web applications`, `community forums`, `chat channels`, `blogs posts`, and `news articles`.

This project is a **work-in-progress** and does not completely work as suggested by this document. Parts of the system work as suggested, other parts are still being worked on.

## Keywords
Example data, mocking library, offline, test data, realistic data, dummy data, fake data

## About
**JSON**

## Benefits

## Features
- 📡 **Offline**: Continue app development, even without an internet connection

## Limitations
- 🧪 **Experimental**: This project is a **work-in-progress**

## Additional Notes

## Progress Report
See the [to-do list](https://gitlab.com/PiusNyakoojo/example-data/blob/master/TODO.md) for a progress report on remaining tasks.

## Browser Compatibility
example-data supports all browsers that are [ES5-compliant](http://kangax.github.io/compat-table/es5/) (IE8 and below are not supported).

## Screenshots
Explore the public [demo](https://piusnyakoojo.gitlab.io/example-data/) or visit the `test/visual` directory to run the visualizer locally.

## Installation

#### Install the library
```
npm install example-data --save
```

#### Import the library
```js
import * as exampleData from 'example-data'
```

#### Or import the browser script
```html
<script src="https://unpkg.com/example-data@latest/dist/library.js"></script>
```

## Example Usecases
For working demos that use this library, please visit the [examples](https://gitlab.com/PiusNyakoojo/example-data/tree/master/examples) directory.

## API Reference

## Documentation
To check out project-related documentation, please visit [docs](https://gitlab.com/PiusNyakoojo/example-data/blob/master/docs/README.md). See [metrics](https://gitlab.com/PiusNyakoojo/example-data/blob/master/docs/metrics.md) for performance-related documentation.

## Contributing
Everyone can contribute. All are welcome. Please see [contributing guide](https://gitlab.com/PiusNyakoojo/example-data/blob/master/CONTRIBUTING.md).

## Acknowledgements

### Software Dependencies
Tooling        |
-----------------|
[TypeScript](https://www.typescriptlang.org/) |

## Learn More
🎬 Watch a video on how to use the visualizer<br>
🎬 Watch a video on how to use this library<br>
🎬 Watch a video on how this library is made<br>

## Related Work
Some well-known **tools for generating example data** include: [JSON Generator](https://www.json-generator.com/), [Mockaroo](https://www.mockaroo.com/), [json-data-generator](https://github.com/everwatchsolutions/json-data-generator), [WireMock](http://wiremock.org/) [example-data](https://gitlab.com/piusnyakoojo/example-data)

## License
MIT
